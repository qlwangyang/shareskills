/**
 * 
 */
package com.coffee.project.shareskills.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.coffee.project.shareskills.dao.PublishedSkillMapper;
import com.coffee.project.shareskills.domain.vo.PublishedSkillsVO;
import com.coffee.project.shareskills.service.PublishedSkillService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;


/**
 * @功能
 * @作者 闫健舒
 * @时间 2018年9月17日 下午2:37:06
 * @版本 ver 1.0.0
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
		Exception.class }, readOnly = true, timeout = 2000)

@Service
public class PublishedSkillServiceImpl implements PublishedSkillService {
	@Autowired
	private PublishedSkillMapper publishedSkillMapper;
	@Override
	public List<PublishedSkillsVO> ListAll() {
		
		return publishedSkillMapper.ListAll();

	}
	

	/**
	 * 分页查询
	 */
	@Override
	public Page<PublishedSkillsVO> listByPage(int pageNum, int pageSize) {
		Page<PublishedSkillsVO> page = PageHelper.startPage(pageNum, pageSize);
		publishedSkillMapper.ListAll();
		return page;
		
	}
	
}
