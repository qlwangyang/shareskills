package com.coffee.project.shareskills.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coffee.project.shareskills.dao.UserMapper;
import com.coffee.project.shareskills.domain.User;
import com.coffee.project.shareskills.service.UserService;

/** 
 * @功能： 
 * @作者: 余敏 
 * @地点: 成都北大青鸟
 * @时间： 2018年9月17日上午10:04:27  
 * @版本: ver 1.0.0
 */
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public List<User> showUsers(int pageIndex, int pageSize) {
		int start = (pageIndex - 1) * pageSize;
		return userMapper.getAll(start, pageSize);
	}
	@Override
	public User getById(Integer id) {
		
		return userMapper.getById(id);
	}
	
	@Override
	public boolean update(User user) {
		// TODO Auto-generated method stub
		return userMapper.update(user) > 0;
	}
	
	
	
	
	
	

}
