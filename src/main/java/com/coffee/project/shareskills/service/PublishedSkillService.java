
package com.coffee.project.shareskills.service;

import java.util.List;

import com.coffee.project.shareskills.domain.vo.PublishedSkillsVO;
import com.github.pagehelper.Page;



/**
 * @功能
 * @作者 闫健舒
 * @时间 2018年9月17日 下午2:34:53
 * @版本 ver 1.0.0
 */
public interface PublishedSkillService {
	
	List<PublishedSkillsVO> ListAll();
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	Page<PublishedSkillsVO> listByPage(int pageNum,int pageSize);
}
