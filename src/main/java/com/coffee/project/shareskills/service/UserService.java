package com.coffee.project.shareskills.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.coffee.project.shareskills.domain.User;

/** 
 * @功能： 
 * @作者: 余敏 
 * @地点: 成都北大青鸟
 * @时间： 2018年9月17日上午10:03:07  
 * @版本: ver 1.0.0
 */
public interface UserService {

	User getById(Integer id);
	
	
	List<User> showUsers(int pageIndex, int pageSize);
	
	boolean update(User user);
}
