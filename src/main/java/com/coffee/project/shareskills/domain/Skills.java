package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Skills implements Serializable{
	// 
	private Integer id;
	// 
	private Integer userId;
	// 
	private Integer skillTypeId;
	// 
	private String description;
	// 
	private Integer status;
	
	@Override
	public String toString() {
		return "Skills [id=" + id + ", userId=" + userId + ", skillTypeId=" + skillTypeId + ", description="
				+ description + ", status=" + status + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSkillTypeId() {
		return skillTypeId;
	}

	public void setSkillTypeId(Integer skillTypeId) {
		this.skillTypeId = skillTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
