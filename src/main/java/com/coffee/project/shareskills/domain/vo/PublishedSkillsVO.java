
package com.coffee.project.shareskills.domain.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PublishedSkillsVO implements Serializable{
	
	private String name; 
	
	private String headUrl;
	
	private double price ; 
	
	private String skillType; 
	
	private String description;
	
	private String city;
	
	private String exchangeMeans; 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSkillType() {
		return skillType;
	}

	public void setSkillType(String skillType) {
		this.skillType = skillType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getExchangeMeans() {
		return exchangeMeans;
	}

	public void setExchangeMeans(String exchangeMeans) {
		this.exchangeMeans = exchangeMeans;
	}

	@Override
	public String toString() {
		return "ShowPublishedSkills [name=" + name + ", headUrl=" + headUrl + ", price=" + price + ", skillType=" + skillType
				+ ", description=" + description + ", city=" + city + ", exchangeMeans=" + exchangeMeans + "]";
	}
	
}
