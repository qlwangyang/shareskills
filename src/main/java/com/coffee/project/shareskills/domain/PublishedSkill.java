package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class PublishedSkill implements Serializable{
	// 编号
	private Integer id;
	// 发布用户编号
	private Integer userId;
	// 技能类型编号
	private Integer skillTypeId;
	// 描述
	private String description; 
	// 地址编号
	private Integer addressId;
	// 交易方式编号
	private Integer exchangeMeansId;
	// 价格
	private Double price;
	// 发布时间
	private Date publishTime;
	// 上架持续时间
	private Date keepTime;
	// 用户状态编号
	private Integer userStatusId;
	
	@Override
	public String toString() {
		return "PublishedSkill [id=" + id + ", userId=" + userId + ", skillTypeId=" + skillTypeId + ", description="
				+ description + ", addressId=" + addressId + ", exchangeMeansId=" + exchangeMeansId + ", price=" + price
				+ ", publishTime=" + publishTime + ", keepTime=" + keepTime + ", userStatusId=" + userStatusId + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSkillTypeId() {
		return skillTypeId;
	}

	public void setSkillTypeId(Integer skillTypeId) {
		this.skillTypeId = skillTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public Integer getExchangeMeansId() {
		return exchangeMeansId;
	}

	public void setExchangeMeansId(Integer exchangeMeansId) {
		this.exchangeMeansId = exchangeMeansId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public Date getKeepTime() {
		return keepTime;
	}

	public void setKeepTime(Date keepTime) {
		this.keepTime = keepTime;
	}

	public Integer getUserStatusId() {
		return userStatusId;
	}

	public void setUserStatusId(Integer userStatusId) {
		this.userStatusId = userStatusId;
	}
}
