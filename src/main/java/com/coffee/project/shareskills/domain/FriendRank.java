package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FriendRank implements Serializable{
	// 编号
	private Integer id;
	// 级别名称
	private String name;
	
	@Override
	public String toString() {
		return "FriendRank [id=" + id + ", name=" + name + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
