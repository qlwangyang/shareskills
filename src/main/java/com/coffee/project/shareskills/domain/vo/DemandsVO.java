package com.coffee.project.shareskills.domain.vo;


import java.io.Serializable;
import java.sql.Time;


@SuppressWarnings("serial")
public class DemandsVO implements Serializable{
	
	private String nickname;
	private Integer price;
	private String description;
	private Time publishTime;
	private Integer keepTime;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Time getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Time publishTime) {
		this.publishTime = publishTime;
	}

	public Integer getKeepTime() {
		return keepTime;
	}

	public void setKeepTime(Integer keepTime) {
		this.keepTime = keepTime;
	}

	@Override
	public String toString() {
		return "ShowDemands [nickname=" + nickname + ", price=" + price + ", description=" + description
				+ ", publishTime=" + publishTime + ", keepTime=" + keepTime + "]";
	}

}