package com.coffee.project.shareskills.domain;

import java.io.Serializable;


@SuppressWarnings("serial")
public class ExchangeMeans implements Serializable{
	// 编号
	private Integer id;
	// 方式
	private String mean;

	@Override
	public String toString() {
		return "ExchangeMeans [id=" + id + ", mean=" + mean + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMean() {
		return mean;
	}

	public void setMean(String mean) {
		this.mean = mean;
	}
	
	
}
