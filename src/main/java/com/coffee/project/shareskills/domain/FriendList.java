package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FriendList implements Serializable{
	// 编号
	private Integer id;
	// 好友 id
	private Integer friendId;
	// 好友备注
	private String name;
	// 分组id
	private Integer friendGroupId;

	@Override
	public String toString() {
		return "FriendList [id=" + id + ", friendId=" + friendId + ", name=" + name + ", friendGroupId=" + friendGroupId
				+ "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFriendId() {
		return friendId;
	}

	public void setFriendId(Integer friendId) {
		this.friendId = friendId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFriendGroupId() {
		return friendGroupId;
	}

	public void setFriendGroupId(Integer friendGroupId) {
		this.friendGroupId = friendGroupId;
	}
}
