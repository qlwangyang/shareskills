package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PersonalMessage implements Serializable{
	// 编号
	private Integer id;
	// 信息编号
	private Integer messageId;
	// 发送对象编号
	private Integer toUserId;
	// 状态
	private Integer status;

	@Override
	public String toString() {
		return "PersonalMessage [id=" + id + ", messageId=" + messageId + ", toUserId=" + toUserId + ", status="
				+ status + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public Integer getToUserId() {
		return toUserId;
	}

	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
