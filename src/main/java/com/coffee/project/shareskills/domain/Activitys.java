package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Activitys implements Serializable{
	// 编号
	private Integer id;
	// 申请者编号
	private Integer userId;
	// 活动标题
	private String title;
	// 活动内容
	private String content;
	// 活动地址
	private String address;
	// 活动时间
	private Date activityTime;
	// 发布时间
	private Date publishedTime;
	// 截止时间
	private Date endTime;
	// 最大参与人数
	private Integer maxUser;
	// 审核状态编号
	private Integer auditingStatusId;

	@Override
	public String toString() {
		return "Activitys [id=" + id + ", userId=" + userId + ", title=" + title + ", content=" + content + ", address="
				+ address + ", activityTime=" + activityTime + ", publishedTime=" + publishedTime + ", endTime="
				+ endTime + ", maxUser=" + maxUser + ", auditingStatusId=" + auditingStatusId + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}

	public Date getPublishedTime() {
		return publishedTime;
	}

	public void setPublishedTime(Date publishedTime) {
		this.publishedTime = publishedTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getMaxUser() {
		return maxUser;
	}

	public void setMaxUser(Integer maxUser) {
		this.maxUser = maxUser;
	}

	public Integer getAuditingStatusId() {
		return auditingStatusId;
	}

	public void setAuditingStatusId(Integer auditingStatusId) {
		this.auditingStatusId = auditingStatusId;
	}
}
