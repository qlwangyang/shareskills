package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ActivityActors implements Serializable{
	// 编号
	private Integer id;
	// 活动编号
	private Integer activityId;
	// 参与者编号
	private Integer actorId;

	@Override
	public String toString() {
		return "Activity_actors [id=" + id + ", activityId=" + activityId + ", actorId=" + actorId + "]";
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}
}
