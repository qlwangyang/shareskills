package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AccountType implements Serializable {
	// 编号
	private Integer id;
	// 类型
	private String type;

	@Override
	public String toString() {
		return "AccountType [id=" + id + ", type=" + type + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
