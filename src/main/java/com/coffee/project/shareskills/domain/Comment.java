package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Comment implements Serializable{
	// 编号
	private Integer id;
	// 发表用户id
	private Integer fromUserId;
	// 评论对象id
	private Integer toId;
	// 评论对象类型
	private Integer targetTypeId;
	// 评论内容
	private String message;
	// 评论时间
	private Date time;

	@Override
	public String toString() {
		return "comment [id=" + id + ", fromUserId=" + fromUserId + ", toId=" + toId + ", targetTypeId=" + targetTypeId
				+ ", message=" + message + ", time=" + time + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Integer getToId() {
		return toId;
	}

	public void setToId(Integer toId) {
		this.toId = toId;
	}

	public Integer getTargetTypeId() {
		return targetTypeId;
	}

	public void setTargetTypeId(Integer targetTypeId) {
		this.targetTypeId = targetTypeId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
