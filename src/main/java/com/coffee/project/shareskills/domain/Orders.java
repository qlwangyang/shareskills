package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Orders implements Serializable{
	// 编号
	private Integer id;
	// 订单编号
	private Integer orderNumber;
	// 下单用户编号
	private Integer fromUserId;
	// 下单对象用户编号
	private Integer toUserId;
	// 发布的技能编号
	private Integer publishedSkillId;
	// 下单时间
	private Date submitTime;
	// 订单状态编号
	private Integer orderStatusId;

	@Override
	public String toString() {
		return "Orders [id=" + id + ", orderNumber=" + orderNumber + ", fromUserId=" + fromUserId + ", toUserId="
				+ toUserId + ", publishedSkillId=" + publishedSkillId + ", submitTime=" + submitTime
				+ ", orderStatusId=" + orderStatusId + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Integer getToUserId() {
		return toUserId;
	}

	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}

	public Integer getPublishedSkillId() {
		return publishedSkillId;
	}

	public void setPublishedSkillId(Integer publishedSkillId) {
		this.publishedSkillId = publishedSkillId;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public Integer getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(Integer orderStatusId) {
		this.orderStatusId = orderStatusId;
	}
}
 