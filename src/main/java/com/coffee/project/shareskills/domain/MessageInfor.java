package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class MessageInfor implements Serializable{
	// 编号
	private Integer id;
	// 发出着用户编号
	private Integer fromUserId;
	// 内容
	private String message;
	// 消息类型编号
	private Integer messageTypeId;
	// 发送时间
	private Date sendTime;

	@Override
	public String toString() {
		return "MessageInfor [id=" + id + ", fromUserId=" + fromUserId + ", message=" + message + ", messageTypeId="
				+ messageTypeId + ", sendTime=" + sendTime + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getMessageTypeId() {
		return messageTypeId;
	}

	public void setMessageTypeId(Integer messageTypeId) {
		this.messageTypeId = messageTypeId;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
}
