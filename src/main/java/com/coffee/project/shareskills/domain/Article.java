package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Article implements Serializable{
	// 编号
	private Integer id;
	// 发表人编号
	private Integer userId;
	// 标题
	private String title;
	// 文章内容
	private String content;
	// 发表时间
	private Date time;
	// 阅读量
	private Integer readNum;
	// 文章等级
	private Integer level;
	
	@Override
	public String toString() {
		return "Article [id=" + id + ", userId=" + userId + ", title=" + title + ", content=" + content + ", time="
				+ time + ", readNum=" + readNum + ", level=" + level + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getReadNum() {
		return readNum;
	}

	public void setReadNum(Integer readNum) {
		this.readNum = readNum;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
}
