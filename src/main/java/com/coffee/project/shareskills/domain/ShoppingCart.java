package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ShoppingCart implements Serializable{
	// 编号
	private Integer id;
	// 用户编号
	private Integer userId;
	// 发布技能编号
	private Integer publishedSkillId;
	
	@Override
	public String toString() {
		return "ShoppingCart [id=" + id + ", userId=" + userId + ", publishedSkillId=" + publishedSkillId + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPublishedSkillId() {
		return publishedSkillId;
	}

	public void setPublishedSkillId(Integer publishedSkillId) {
		this.publishedSkillId = publishedSkillId;
	}
}
