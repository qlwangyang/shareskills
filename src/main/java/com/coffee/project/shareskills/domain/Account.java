package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Account implements Serializable {
	// 编号
	private Integer id;
	// 用户名
	private String userName;
	// 密码
	private String password;
	// 账号状态编号
	private Integer accountStatusId;
	// 账号类型编号
	private Integer typeId;
	// 昵称
	private String nickname;
	// 注册时间
	private Date registerTime;
	// 等级编号
	private Integer levelId;
	// 积分
	private Integer score;
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", userName=" + userName + ", password=" + password + ", accountStatusId="
				+ accountStatusId + ", typeId=" + typeId + ", nickname=" + nickname + ", registerTime=" + registerTime
				+ ", levelId=" + levelId + ", score=" + score + "]";
	}

	// getters and setters start
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAccountStatusId() {
		return accountStatusId;
	}

	public void setAccountStatusId(Integer accountStatusId) {
		this.accountStatusId = accountStatusId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public Integer getLevelId() {
		return levelId;
	}

	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	// getters and setters end
}
