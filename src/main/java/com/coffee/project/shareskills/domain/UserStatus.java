package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserStatus implements Serializable{
	// 
	private Integer id;
	// 
	private String status;
	
	@Override
	public String toString() {
		return "UserStatus [id=" + id + ", status=" + status + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
