package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ActivityStatus implements Serializable{
	// 编号
	private Integer id;
	// 活动状态
	private String status;
	
	@Override
	public String toString() {
		return "AccountStatus [id=" + id + ", status=" + status + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
