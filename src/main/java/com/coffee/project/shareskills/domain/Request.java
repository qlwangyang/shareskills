package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Request implements Serializable{
	// 编号
	private Integer id;
	// 用户编号
	private Integer userId;
	// 技能编号
	private Integer skillId;
	// 
	private String certificate; 
	// 个人介绍
	private String personalDescription;
	// 技能介绍
	private String skillDescription;
	// 审核状态编号
	private Integer auditingStatusId;
	// 申请时间
	private Date submitTime;
	// 审核时间
	private Date auditingTime;
	// ����˱��
	private Integer managerId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}



	public Integer getAuditingStatusId() {
		return auditingStatusId;
	}

	public void setAuditingStatusId(Integer auditingStatusId) {
		this.auditingStatusId = auditingStatusId;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public Date getAuditingTime() {
		return auditingTime;
	}

	public void setAuditingTime(Date auditingTime) {
		this.auditingTime = auditingTime;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getPersonalDescription() {
		return personalDescription;
	}

	public void setPersonalDescription(String personalDescription) {
		this.personalDescription = personalDescription;
	}

	public String getSkillDescription() {
		return skillDescription;
	}

	public void setSkillDescription(String skillDescription) {
		this.skillDescription = skillDescription;
	}
}
