package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Demands implements Serializable{
	// 编号
	private Integer id;
	// 发布用户id
	private Integer userId;
	// 需求技能类型id
	private Integer skillTypeId;
	// 需求描述
	private String description;
	// 任务地址编号
	private Integer addressId;
	// 交易方式
	private Integer exchangeMeansId;
	// 价格
	private Double price;
	// 发布时间
	private Date publishTime;
	// 上架持续时间
	private Integer keepTime;

	@Override
	public String toString() {
		return "Demands [id=" + id + ", userId=" + userId + ", skillTypeId=" + skillTypeId + ", description="
				+ description + ", addressId=" + addressId + ", exchangeMeansId=" + exchangeMeansId + ", price=" + price
				+ ", publishTime=" + publishTime + ", keepTime=" + keepTime + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSkillTypeId() {
		return skillTypeId;
	}

	public void setSkillTypeId(Integer skillTypeId) {
		this.skillTypeId = skillTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public Integer getExchangeMeansId() {
		return exchangeMeansId;
	}

	public void setExchangeMeansId(Integer exchangeMeansId) {
		this.exchangeMeansId = exchangeMeansId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public Integer getKeepTime() {
		return keepTime;
	}

	public void setKeepTime(Integer keepTime) {
		this.keepTime = keepTime;
	}
}
