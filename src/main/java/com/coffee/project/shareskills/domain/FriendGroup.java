package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FriendGroup implements Serializable{
	// 编号
	private Integer id;
	// 用户id
	private Integer userId;
	// 分组名称
	private String name;
	// 分组级别编号
	private Integer friendRankId;
	
	@Override
	public String toString() {
		return "FriendGroup [id=" + id + ", userId=" + userId + ", name=" + name + ", friendRankId=" + friendRankId
				+ "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFriendRankId() {
		return friendRankId;
	}

	public void setFriendRankId(Integer friendRankId) {
		this.friendRankId = friendRankId;
	}
}
