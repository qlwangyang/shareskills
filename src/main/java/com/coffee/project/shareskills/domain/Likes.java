package com.coffee.project.shareskills.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Likes implements Serializable{
	// 编号
	private Integer id;
	// 点赞对象编号
	private Integer toId;
	// 操作用户id
	private Integer userId;
	// 点赞对象类型编号
	private Integer likeType;
	
	@Override
	public String toString() {
		return "Likes [id=" + id + ", toId=" + toId + ", userId=" + userId + ", likeType=" + likeType + "]";
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getToId() {
		return toId;
	}
	public void setToId(Integer toId) {
		this.toId = toId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getLikeType() {
		return likeType;
	}
	public void setLikeType(Integer likeType) {
		this.likeType = likeType;
	}
}
