package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@SuppressWarnings("serial")
public class User implements Serializable{
	// 
	private Integer id;
	// 头像地址
	private String headUrl;
	//
	private String realName;
	// 
	private Integer sex;
	// 
	// @DateTimeFormat(pattern="yy-MM-dd")
	private Date birthday;
	
	private String birthdayStr;
	// 
	private String email;
	//
	private String job;
	// 
	private String identity;
	// 
	private String introduction;
	// 
	private Integer userStatusId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/*@DateTimeFormat(pattern=("yy-MM-dd"))
	@JsonFormat(pattern=("yy-MM-dd"),timezone="GMT+8")*/
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
		this.birthdayStr = birthday.toString();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Integer getUserStatusId() {
		return userStatusId;
	}

	public void setUserStatusId(Integer userStatusId) {
		this.userStatusId = userStatusId;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", headUrl=" + headUrl + ", realName=" + realName + ", sex=" + sex + ", birthday="
				+ birthday + ", email=" + email + ", job=" + job + ", identity=" + identity + ", introduction="
				+ introduction + ", userStatusId=" + userStatusId + "]";
	}

	public String getBirthdayStr() {
		return birthdayStr;
	}

	public void setBirthdayStr(String birthdayStr) {
		this.birthdayStr = birthdayStr;
	}


	
}
