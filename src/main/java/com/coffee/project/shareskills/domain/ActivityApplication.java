package com.coffee.project.shareskills.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class ActivityApplication implements Serializable{
	// 编号
	private Integer id;
	// 活动编号
	private Integer activityId;
	// 审核时间
	private Date auditingTime;
	// 审核人编号
	private Integer managerId;

	@Override
	public String toString() {
		return "ActivityApplication [id=" + id + ", activityId=" + activityId + ", auditingTime=" + auditingTime
				+ ", managerId=" + managerId + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Date getAuditingTime() {
		return auditingTime;
	}

	public void setAuditingTime(Date auditingTime) {
		this.auditingTime = auditingTime;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}
}
