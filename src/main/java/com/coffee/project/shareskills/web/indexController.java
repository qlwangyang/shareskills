/**
 * 
 */
package com.coffee.project.shareskills.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @功能
 * @作者 闫健舒
 * @时间 2018年9月17日 上午11:54:34
 * @版本 ver 1.0.0
 */
@Controller
public class indexController {
	
		@RequestMapping(path ="/index",method = RequestMethod.GET)
		public String publishedSkills() {
			
			return "index";
		}
		
	}

