package com.coffee.project.shareskills.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coffee.project.shareskills.domain.User;
import com.coffee.project.shareskills.service.UserService;

/**
 * @功能：
 * 
 * @作者: 余敏
 * @地点: 成都北大青鸟 @时间： 2018年9月17日上午9:20:16
 * @版本: ver 1.0.0
 */
@Controller
public class PersonalCentralityController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/personalCentrality", method = RequestMethod.GET)
	public String talentCentrality(HttpSession session) {
		User user = userService.getById(1);
		session.setAttribute("user", user);
		return "personalCentrality";
		

	}

	@RequestMapping(path = "/talentCentrality/update", method = RequestMethod.POST)
	public String create(User user, HttpServletRequest request) {

		userService.update(user);
		String url = request.getHeader("Referer");
		return "redirect:" + url;
	}
}
