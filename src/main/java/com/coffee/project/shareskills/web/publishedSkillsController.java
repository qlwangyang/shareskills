/**
 * 
 */
package com.coffee.project.shareskills.web;



import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.coffee.project.shareskills.domain.vo.PublishedSkillsVO;
import com.coffee.project.shareskills.service.PublishedSkillService;


/**
 * @功能
 * @作者 闫健舒
 * @时间 2018年9月17日 上午9:56:25
 * @版本 ver 1.0.0
 */

@Controller
public class publishedSkillsController {
	@Autowired
	private PublishedSkillService publishedSkillSerivce;
	
	
	@RequestMapping(path ="/publishedSkills",method = RequestMethod.GET)
	public Object publishedSkills(HttpSession session) {
		
		List<PublishedSkillsVO> publishedSkillsVOs = publishedSkillSerivce.ListAll();
		
		session.setAttribute("publishedSkillsVOs", publishedSkillsVOs);
		
		return "publishedSkills";
	}
}
