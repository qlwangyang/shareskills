package com.coffee.project.shareskills.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.coffee.project.shareskills.domain.User;



public interface UserMapper {

	User getById(Integer id);
	
	List<User> getByAccAndPwd(@Param("headUrl") Integer headUrl, @Param("realName")String realName);
	
	List<User> getAll(@Param("start") int start, @Param("pageSize") int pageSize);
	
	int update(User user);
}
