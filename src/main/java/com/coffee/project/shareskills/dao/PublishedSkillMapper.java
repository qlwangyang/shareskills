/**
 * 
 */
package com.coffee.project.shareskills.dao;

import java.util.List;

import com.coffee.project.shareskills.domain.vo.PublishedSkillsVO;



/**
 * @功能
 * @作者 闫健舒
 * @时间 2018年9月17日 下午2:26:49
 * @版本 ver 1.0.0
 */
public interface PublishedSkillMapper {
	/**
	 * 查询全部
	 * @return
	 */
	List<PublishedSkillsVO> ListAll();
	
	
}
