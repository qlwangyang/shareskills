<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eElectronics - HTML eCommerce Template</title>

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="${WEB_PATH }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="${WEB_PATH }/static/css/owl.carousel.css">
    <link rel="stylesheet" href="${WEB_PATH }/static/css/style.css">
    <link rel="stylesheet" href="${WEB_PATH }/static/css/responsive.css">
    <link rel="stylesheet" href="${WEB_PATH }/static/css/publishedSkills.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- 引入导航栏和菜单栏 -->
	<%@include file="layout/navigation.jsp"%>

	<div class="product-big-title-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="product-bit-title text-center">
						<h2>需求任务</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="single-product-area">
		<div class="rng1-1">
			<a href="function:;" class="rng1-2">筛选 <i
				class="fa fa-angle-down" style="font-size: 24px"></i></a>
		</div>
		<div class="rng2">
			<h2>价格</h2>
			<ul>
				<li><a href="#" class="rng2-1">0-300</a></li>
				<li><a href="#" class="rng2-1">301-600</a></li>
				<li><a href="#" class="rng2-1">601-1000</a></li>
				<li><a href="#" class="rng2-1">1000以上</a></li>
			</ul>
			<div class="rng2-2">
				<input type="text" placeholder="起始价格" /> — <input type="text"
					placeholder="终止价格" /> <a href="#">确定</a>
			</div>

		</div>
		<div data-toggle="distpicker" class="address">
			<select data-province="---- 选择省 ----"></select> 
			<select data-city="---- 选择市 ----"></select>
			<ul class="rng3-2">
				<li class="rng3-2-1"><a>综合排序</a></li>
				<!-- <li class="rng3-2-1"><a>见过最多</a></li>
				<li class="rng3-2-1"><a>评分最高</a></li> -->
				<li class="rng3-2-1"><a href="/showAll/demands.html?type=publish_time">发布时间</a></li>
				<li class="rng3-2-1"><a href="/showAll/demands.html?type=price">价格</a></li>
			</ul>
		</div>
		
		<c:forEach items="${showDemand }" var="p">
			<div class="rng4 col-md-8 col-md-offset-2" >
				<img src="${WEB_PATH}/static/img/head/萌萌哒.png" />
				<div class="rng4-1">
					
					<h3>${p.nickname}<h6><span>上架时间：</span>${p.keepTime}<span>小时</span>
						</h6>
					</h3>
					<h4>${p.description}</h4>
					<span class="rng4-1-1"></span><span
						class="rng4-1-2">发布时间：<i>${p.publishTime}</i></span>
					<h2>${p.price}<span>元/次</span>
					</h2>
					
				</div>
			</div>
		</c:forEach>
	</div>


	<div class="footer-top-area">
		<div class="zigzag-bottom"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-about-us">
						<h2>
							e<span>Electronics</span>
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
							Perferendis sunt id doloribus vero quam laborum quas alias
							dolores blanditiis iusto consequatur, modi aliquid eveniet
							eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit,
							debitis, quisquam. Laborum commodi veritatis magni at?</p>
						<div class="footer-social">
							<a href="#" target="_blank"><i class="fa fa-facebook"></i></a> <a
								href="#" target="_blank"><i class="fa fa-twitter"></i></a> <a
								href="#" target="_blank"><i class="fa fa-youtube"></i></a> <a
								href="#" target="_blank"><i class="fa fa-linkedin"></i></a> <a
								href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="footer-menu">
						<h2 class="footer-wid-title">User Navigation</h2>
						<ul>
							<li><a href="">My account</a></li>
							<li><a href="">Order history</a></li>
							<li><a href="">Wishlist</a></li>
							<li><a href="">Vendor contact</a></li>
							<li><a href="">Front page</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="footer-menu">
						<h2 class="footer-wid-title">Categories</h2>
						<ul>
							<li><a href="">Mobile Phone</a></li>
							<li><a href="">Home accesseries</a></li>
							<li><a href="">LED TV</a></li>
							<li><a href="">Computer</a></li>
							<li><a href="">Gadets</a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="footer-newsletter">
						<h2 class="footer-wid-title">Newsletter</h2>
						<p>Sign up to our newsletter and get exclusive deals you wont
							find anywhere else straight to your inbox!</p>
						<div class="newsletter-form">
							<input type="email" placeholder="Type your email"> <input
								type="submit" value="Subscribe">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="copyright">
						<p>
							&copy; 2015 eElectronics. All Rights Reserved. More Templates <a
								href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a>
							- Collect from <a href="http://www.cssmoban.com/" title="网页模板"
								target="_blank">网页模板</a>
						</p>
					</div>
				</div>

				<div class="col-md-4">
					<div class="footer-card-icon">
						<i class="fa fa-cc-discover"></i> <i class="fa fa-cc-mastercard"></i>
						<i class="fa fa-cc-paypal"></i> <i class="fa fa-cc-visa"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="${WEB_PATH }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="${WEB_PATH }/static/js/owl.carousel.min.js"></script>
    <script src="${WEB_PATH }/static/js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="${WEB_PATH }/static/js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="${WEB_PATH }/static/js/main.js"></script>
    
	<script src="${WEB_PATH }/static/Address/distpicker.data.min.js"></script>
	<script src="${WEB_PATH }/static/Address/distpicker.min.js"></script>
	<script src="${WEB_PATH }/static/Address/main.js"></script>
	
	<script src="${WEB_PATH }/static/js/publishedSkills.js"></script>
	
	<script>
		$(function(){
			$("#a-demand").css("background-color","#222");
		});
	
	</script>
</body>
</html>