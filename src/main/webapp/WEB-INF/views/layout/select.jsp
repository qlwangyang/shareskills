<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<div class="product-big-title-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="product-bit-title text-center">
					<h2>技能显示</h2>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="rng1-1">
		<a href="function:;" class="rng1-2">筛选 <i class="fa fa-angle-down"
			style="font-size: 24px"></i></a>
	</div>
	<div class="rng2">
		<h2>价格</h2>
		<ul>
			<li><a href="#" class="rng2-1">0-300</a></li>
			<li><a href="#" class="rng2-1">301-600</a></li>
			<li><a href="#" class="rng2-1">601-1000</a></li>
			<li><a href="#" class="rng2-1">1000以上</a></li>
		</ul>
		<div class="rng2-2">
			<input type="text" placeholder="起始价格" /> — <input type="text"
				placeholder="终止价格" /> <a href="#">确定</a>
		</div>
	</div>
	<div data-toggle="distpicker" class="address">
		<select data-province="---- 选择省 ----"></select> <select
			data-city="---- 选择市 ----"></select> <select
			data-district="---- 选择区 ----"></select>
		<ul class="rng3-2">
			<li class="rng3-2-1"><a href="/showAll/publishedSkills.html">综合排序</a></li>
			<li class="rng3-2-1"><a>交易方式</a></li>
			<li class="rng3-2-1"><a>地址</a></li>
			<li class="rng3-2-1"><a>类型</a></li>
			<li class="rng3-2-1"><a id="prices">价格</a></li>
			<!-- <li class="rng3-2-1"><button type="button" id="prices">价格</button></li> -->
		</ul>
	</div>