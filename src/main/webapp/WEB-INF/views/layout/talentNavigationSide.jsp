<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
	<div id="side-menu" class="col-sm-2">
		<ul class="nav" id="side-menu-collapse">
			<li>
				<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-info">
					<i class="fa fa-dashboard"></i> 技能管理
				</a>
				<div id="collapse-info" class="collapse">
					<ul class="nav">
						<li><a href="function:;">我的技能</a></li>
						<li><a href="function:;">技能申请</a></li>
						<li><a href="function:;">上架技能</a></li>
					</ul>
				</div>
			</li>
		    <li>
		    	<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-safe">
		    		<i class="fa fa-table"></i> 达人社区
		    	</a>
		    	<div id="collapse-safe" class="collapse">
					<ul class="nav">
						<li><a href="function:;">社区论坛</a></li>
						<li><a href="function:;">社区活动</a></li>
					</ul>
				</div>
		    </li>
		    <li>
		    	<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-order">
		    		<i class="fa fa-table"></i> 订单管理
		    	</a>
		    	<div id="collapse-order" class="collapse">
					<ul class="nav">
						<li><a href="function:;">待确认订单</a></li>
						<li><a href="function:;">交易中订单</a></li>
						<li><a href="function:;">历史订单</a></li>
					</ul>
				</div>
		    </li>
		</ul>    
	</div>