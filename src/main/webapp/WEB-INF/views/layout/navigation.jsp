<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="user-menu">
						<ul id="service-menu">
							<li class="dropdown dropdown-small">
							 	<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
									户外服务  <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="function:;">摄影</a></li>
									<li><a href="#">旅游</a></li>
									<li><a href="#">游泳</a></li>
									<li><a href="#">跑腿服务</a></li>
									<li><a href="#">其它</a></li>
								</ul>
							</li>
							<li class="dropdown dropdown-small">
							 	<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
									教育服务  <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="function:;">一对一教学</a></li>
									<li><a href="#">小班教学</a></li>
									<li><a href="#">大班教学</a></li>
									<li><a href="#">视频教学</a></li>
									<li><a href="#">其它</a></li>
								</ul>
							</li>
							<li class="dropdown dropdown-small">
							 	<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
									生活服务  <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="function:;">水电维修</a></li>
									<li><a href="#">家具维修</a></li>
									<li><a href="#">私人厨师</a></li>
									<li><a href="#">家政服务</a></li>
									<li><a href="#">收纳</a></li>
									<li><a href="#">其它</a></li>
								</ul>
							</li>
							<li class="dropdown dropdown-small">
							 	<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
									互联网服务  <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="function:;">游戏代练</a></li>
									<li><a href="#">软件教程</a></li>
									<li><a href="#">网站建设</a></li>
									<li><a href="#">其它</a></li>
								</ul>
							</li>
							<li>
								<button class="btn btn-default" type="button" onclick="window.location.href='${WEB_PATH}/showAll/publishedSkills.html'"
								id="dropdownMenu5">
									全部服务 
								</button>
							</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-5">
                    <div class="header-right">
                    	<c:choose>
                        	<c:when test="${empty user}">
                        		<!-- 用户未登录时的导航栏显示 -->
                        		<ul class="list-unstyled list-inline user-menu-right">
                        			<li><a data-toggle="modal" data-target="#user-login-modal"><i class="fa fa-user"></i> 登录</a></li>
                        			<li><a data-toggle="modal" data-target="#user-register-modal"><i class="fa fa-pencil"></i> 注册</a></li>
                        			<li><a><i class="fa fa-group"></i> 关于我们</a></li>
                        		</ul>
                        	</c:when>
                        	<c:otherwise>
                        		<!-- 用户登录后的导航栏显示效果 -->
                        		<ul class="list-unstyled list-inline user-menu-right">
		                            <li class="dropdown dropdown-small">
		                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="function:;">
		                                	${account.nickname },欢迎您 <b class="caret"></b>
		                                </a>
		                                <ul class="dropdown-menu">
		                                    <li><a onclick="window.location.href='${WEB_PATH }/personal/centrality.html'">个人中心</a></li>
		                                    <li><a data-toggle="modal" data-target="#add-address-modal">
		                                    	新增地址</a></li>
		                                    <li><a href="function:;">我的好友</a></li>
		                                    <li><a href="function:;">我的收藏</a></li>
		                                    <li><a href="function:;">我的关注</a></li>
		                                </ul>
		                            </li>
		                            <li>
		                                <a href="${WEB_PATH }/logoff.html">注销</a>
		                            </li>
		                            <li>
		                                <a href="#"><i class="fa fa-comment"></i></a>
		                            </li>
		                            <li><a><i class="fa fa-group"></i> 关于我们</a></li>
                        			
		                        </ul>  		                                		
                        	</c:otherwise>
                       	</c:choose>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="logo">
                        <h1><a href="${WEB_PATH }/index.html"><i class="fa "></i><span>shareSkill</span></a></h1>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="shopping-item col-sm-8 col-sm-offset-2" id="publish-demand">
                    	<a data-toggle="modal" data-target="#publish-demand-modal">发布需求</a>
                    </div>
                </div>
                <!-- 这里需要进行判断 当用户登录后获取其账户类型，若为技能达人用户，则a标签内容显示为我的技能之家 -->
                <div class="col-sm-3">
                    <div class="shopping-item col-sm-10 col-sm-offset-1" id="be-skill-talent">
						<!-- 普通用户、游客显示 -->
						<c:if test="${account.typeId ne 3 }">
							<a data-toggle="modal" data-target="#be-skill-talent-modal">成为技能达人</a>
						</c:if>
						<!-- 技能达人用户显示 -->
						<c:if test="${account.typeId eq 3 }">
							<a href="${WEB_PATH }/talent/centrality.html">我的达人之家</a>
						</c:if>
					</div>
                </div>
                <div class="col-sm-3">
                    <div class="shopping-item">
                        <a href="${WEB_PATH }/user/cart ">Cart - <span class="cart-amunt">$800</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area" id="xj-navigation">
        <div class="container">
            <div class="row">
                <div class="navbar-header" style="height:60px">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>  
                <div class="navbar-collapse collapse">
	                <div class="col-md-8 col-sm-8">
	                    <ul class="nav navbar-nav" style="height:60px">
	                        <li class="active"><a href="${WEB_PATH }/index.html" id="a-index">首页</a></li>
	                        <li><a href="${WEB_PATH}/showAll/demands.html" id="a-demand">需求任务</a></li>
	                        <li><a href="${WEB_PATH}/showAll/publishedSkills.html" id="a-skill">技能展示</a></li>
	                        <li><a href="cart.jsp">技能达人展示</a></li>
	                        <li><a href="checkout.jsp">购物车</a></li>
	                        <li>
	                          	<div style="margin: 5px 0" class="col-md-4 col-sm-4 visible-xs">
									<div class="input-group">
										<span class="input-group-addon" id="searchBy"> 
											<select style="border: none; height: 32px; border-radius: 15px 0 0 15px">
													<option value="daRen">达人</option>
													<option value="skills">技能</option>
													<option value="demands">需求</option>
											</select>
										</span> 
										<input type="text" class="form-control" placeholder="Search for..."> 
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" style="border-radius: 0 15px 15px 0">
												<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
									<!-- /input-group -->
								</div>
							</li>
	                    </ul>
	                </div>
                    <div style="margin-top:13px" class="col-md-4 col-sm-4 hidden-xs">
						<div class="input-group">
							<span class="input-group-addon" id="searchBy">
								<select style="border:none; height:32px; border-radius:15px 0 0 15px">
									<option value="daRen">达人</option>
									<option value="skills">技能</option>
									<option value="demands">需求</option>
								</select>
							</span>
							<input type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" style="border-radius:0 15px 15px 0"><i class="fa fa-search"></i></button>
							</span>
						</div>
						<!-- /input-group -->
					</div>
                </div>  
            </div>
        </div>
    </div> <!-- End mainmenu area -->
    <!-- 引用用户登录、注册模态框 -->
    <%@include file="modals.jsp" %>
    