<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 登录模态框 -->
<div class="modal fade" id="user-login-modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="fa fa-user"></i> 用户登录
				</h4>
			</div>
			<div class="modal-body">
				<form action="${WEB_PATH }/login1.html" method="post"
					class="form-horizontal" id="user-login-form">
					<div class="form-group">
						<label class="control-label col-md-3">用户名</label>
						<div class="col-md-6">
							<input class="form-control" type="text"
								autocomplete="off" name=userName>
						</div>
						<div class="col-md-3 tip"></div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">密码</label>
						<div class="col-md-6">
							<input class="form-control" type="password"
								name="password">
						</div>
						<div class="col-md-3 tip"></div>
					</div>
					
					<div class="form-group">
           			 	<div class="col-md-6 col-md-offset-3">
              				<label class="checkbox">
                				<input name="remember" id="remember" type="checkbox"> 记住密码
              				</label>
            			</div>
          			</div>
					
					<div class="form-group">
  	    	  			<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
  	    	    			<input class="btn-primary form-control" type="submit" value="登录">
  	    	  			</div>
  	    			</div>
				</form>
			</div>
			<div class="modal-footer">
				没有账号？
      			<a href="">联系管理员</a>
      			<a href="">找回密码</a>
			</div>
		</div>
	</div>
</div>
<!-- 注册模态框 -->
<div class="modal fade" id="user-register-modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="fa fa-user"></i> 用户注册
				</h4>
			</div>
			<div class="modal-body">
				<form action="${WEB_PATH }/register.html" method="post"
					class="form-horizontal" id="user-register-form">
					<div class="form-group">
						<label class="control-label col-md-3">用户名</label>
						<div class="col-md-6">
							<input class="form-control" type="text"
								autocomplete="off" name=userName>
						</div>
						<div class="col-md-3 tip"></div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">密码</label>
						<div class="col-md-6">
							<input class="form-control" type="password"
								name="password">
						</div>
						<div class="col-md-3 tip"></div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">确认密码</label>
						<div class="col-md-6">
							<input class="form-control" type="password"
								name="password">
						</div>
						<div class="col-md-3 tip"></div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">昵称</label>
						<div class="col-md-6">
							<input class="form-control" type="text"
								autocomplete="off" name=nickname>
						</div>
						<div class="col-md-3 tip"></div>
					</div>

					<div class="form-group">
           			 	<div class="col-md-6 col-md-offset-3">
              				<label class="checkbox">
                				<input name="accept" id="accept" type="checkbox"> 同意用户协议
              				</label>
            			</div>
          			</div>
          			
          			<div class="form-group">
  	    	  			<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
  	    	    			<input class="btn-primary form-control" type="submit" value="注册">
  	    	  			</div>
  	    			</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- 发布需求模态框 -->
<div class="modal fade" id="publish-demand-modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="fa fa-user"></i> 发布需求
				</h4>
			</div>
			<div class="modal-body">
				<form action="${WEB_PATH }/publishDemand.html" method="post"
					class="form-horizontal" id="publish-demand-form">
					
					<!-- 隐藏表单域  账户编号-->
					<div>
						<input type="hidden" name="account-id" value="${user.id }"/>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">任务类型</label>
						<div class="col-sm-4">
							<select class="form-control" id="demand-skill-type" name="demand-skill-type">
								<c:forEach items="${types }" var="t">
									<option value="${t.id }">${t.type }</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-5 tip"></div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-3">交易方式</label>
						<div class="col-sm-4">
							<select class="form-control" id="exchange-mean" name="exchange-mean">
								<option value="1">当面交易</option>
								<option value="2">远程交易</option>
								<option value="3">不限</option>
							</select>
						</div>
						<div class="col-sm-5 tip"></div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-3">任务报酬</label>
						<div class="col-sm-4">
							<input class="form-control" type="text"
								autocomplete="off" name="demand-price">
						</div>
						<div class="col-sm-5 tip">元</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-3">任务上架时长</label>
						<div class="col-sm-4">
							<input class="form-control" type="text"
								autocomplete="off" name=keep-time>
						</div>
						<div class="col-sm-5 tip">小时</div>
					</div>		
					
					<div class="form-group">
						<label class="control-label col-sm-3">交易地址</label>
						<div class="col-sm-6">
							<select class="form-control" id="exchange-address" name="exchange-address">
								<c:forEach items="${addresses }" var="a">
									<option value="${a.id }">${a.city }${a.area}${a.street }</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-3 tip">
						</div>
					</div>
          			
					<div class="form-group">
						<label class="control-label col-sm-3">任务描述</label>
						<div class="col-sm-6">
							<textarea rows="5" cols="38" style="resize:none" name="demand-description"></textarea>
						</div>
						<div class="col-sm-3 tip"></div>
					</div>
          			
          			<div class="form-group">
  	    	  			<div class="col-sm-6 col-sm-offset-3">
  	    	    			<input class="btn-primary form-control" type="submit" value="发布任务">
  	    	  			</div>
  	    			</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- 成为技能达人 -->
<div class="modal fade" id="be-skill-talent-modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="fa fa-user"></i> 技能达人申请
				</h4>
			</div>
			<div class="modal-body">
				<form action="${WEB_PATH }/request/skilltalent.html" method="post"
					class="form-horizontal" id="be-skill-talent-form">
					<div class="form-group">
						<label class="control-label col-sm-3">技能类型</label>
						<div class="col-sm-4">
							<select class="form-control" id="skill-type" name="skill-type">
								<c:forEach items="${types }" var="t">
									<option value="${t.id }">${t.type }</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-5 tip"></div>
					</div>		
	
					<!-- 隐藏表单域  账户编号-->
					<div>
						<input type="hidden" name="account-id" value="${user.id }"/>
					</div>
          			
          			<div class="form-group">
						<label class="control-label col-sm-3">技能描述</label>
						<div class="col-sm-6">
							<textarea rows="5" cols="38" style="resize:none" name="skill-description"></textarea>
						</div>
						<div class="col-sm-3 tip"></div>
					</div>
          			
					<div class="form-group">
						<label class="control-label col-sm-3">个人描述</label>
						<div class="col-sm-6">
							<textarea rows="5" cols="38" style="resize:none" name="personal-description"></textarea>
						</div>
						<div class="col-sm-3 tip"></div>
					</div>
          			
          			<div class="form-group">
  	    	  			<div class="col-sm-6 col-sm-offset-3">
  	    	    			<input class="btn-primary form-control" type="submit" value="提交申请">
  	    	  			</div>
  	    			</div>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- 新增地址 -->
<div class="modal fade" id="add-address-modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="fa fa-user"></i> 新增地址
				</h4>
			</div>
			<div class="modal-body">
				<form action="${WEB_PATH }/add/address.html" method="post"
					class="form-horizontal" id="add-address-form">
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-1">
							<div data-toggle="distpicker">
							<select data-province="---- 选择省 ----" class="form-control" name="province"></select> 
							<select data-city="---- 选择市 ----" class="form-control" name="city"></select> 
							<select data-district="---- 选择区 ----" class="form-control" name="area"></select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2">街道</label>
						<div class="col-sm-8">
							<input class="form-control" type="text"
								name="street">
						</div>
						<div class="col-sm-2 tip"></div>
					</div>

          			<div class="form-group">
  	    	  			<div class="col-sm-4 col-sm-offset-4">
  	    	    			<input class="btn-primary form-control" type="submit" value="添加">
  	    	  			</div>
  	    			</div>
				</form>
			</div>
		</div>
	</div>
</div>



