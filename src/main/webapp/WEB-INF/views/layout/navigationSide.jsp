<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

	<div id="side-menu" class="col-sm-2">
		<ul class="nav" id="side-menu-collapse">
			<li>
				<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-info">
					<i class="fa fa-dashboard"></i> 个人信息
				</a>
				<div id="collapse-info" class="collapse">
					<ul class="nav">
						<li><a href="function:;">完善信息</a></li>
						<li><a href="function:;">修改信息</a></li>
						<li><a href="function:;">地址管理</a></li>
					</ul>
				</div>
			</li>
		    <li>
		    	<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-safe">
		    		<i class="fa fa-table"></i> 安全设置
		    	</a>
		    	<div id="collapse-safe" class="collapse">
					<ul class="nav">
						<li><a href="function:;">修改密码</a></li>
						<li><a href="function:;">更改手机绑定</a></li>
						<li><a href="function:;">地址管理</a></li>
					</ul>
				</div>
		    </li>
		    <li>
		    	<a data-toggle="collapse" data-parent="#side-menu-collapse" href="#collapse-order">
		    		<i class="fa fa-table"></i> 订单管理
		    	</a>
		    	<div id="collapse-order" class="collapse">
					<ul class="nav">
						<li><a href="function:;">待付款订单</a></li>
						<li><a href="function:;">交易中订单</a></li>
						<li><a href="function:;">历史订单</a></li>
					</ul>
				</div>
		    </li>
		</ul>    
	</div>
	
