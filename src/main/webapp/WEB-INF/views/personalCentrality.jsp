<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>享技网---用专业成就您的无限需求！</title>

<!-- Google Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="${WEB_PATH }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="${WEB_PATH }/static/css/owl.carousel.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/style.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/responsive.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/navigationSide.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/app.css">
<link rel="stylesheet"
	href="${WEB_PATH }/static/css/talentCentrality.css">

</head>
<body>
	<div>
		<!-- 引入顶部导航栏 -->
		<%@include file="layout/navigation.jsp"%>
		<!-- 引入侧边栏 -->
		<%@include file="layout/navigationSide.jsp"%>
	</div>
	<div class='body-first-now'>
		<form class="form-horizontal" method="post"
			action="${WEB_PATH }/talentCentrality/update">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">姓名:
				</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputEmail3"
						name="realName" value="${user.realName }" readonly="readonly">
				</div>
			</div>
			<div>
				<input type="hidden" class="form-control" id="inputEmail3" name="id"
					value="${user.id }" readonly="readonly">
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">性别:</label>
				<div class="col-sm-2">
					 <select class="form-control" disabled="disabled" name="sex" id="sex">
						<option value="0">男</option>
						<option value="1">女</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">生日:</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputPassword3"
						name="birthday" value="${user.birthday }" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">职业:</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputPassword3"
						name="job" value="${user.job }" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">邮箱:</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputPassword3"
						name="email" value="${user.email }" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">身份证号码:</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="inputPassword3"
						name="identity" value="${user.identity }" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">个人简介:</label>
				<div class="col-sm-5">
					<textarea class="form-control" rows="4" name="introduction"
						id="introduction" style="resize: none" readonly="readonly"></textarea>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					<button type="button" class="btn btn-primary" id="alter-info">修改资料</button>
					<button type="submit" class="btn btn-success" id="alter-confirm"
						disabled="disabled">确认修改</button>
				</div>
			</div>
		</form>
	</div>


	<!-- Latest jQuery form server -->
	<script src="https://code.jquery.com/jquery.min.js"></script>

	<!-- Bootstrap JS form CDN -->
	<script
		src="${WEB_PATH }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

	<!-- jQuery sticky menu -->
	<script src="${WEB_PATH }/static/js/owl.carousel.min.js"></script>
	<script src="${WEB_PATH }/static/js/jquery.sticky.js"></script>

	<!-- jQuery easing -->
	<script src="${WEB_PATH }/static/js/jquery.easing.1.3.min.js"></script>

	<!-- Main Script -->
	<script src="${WEB_PATH }/static/js/main.js"></script>

	<script src="${WEB_PATH }/static/Address/distpicker.data.min.js"></script>
	<script src="${WEB_PATH }/static/Address/distpicker.min.js"></script>
	<script src="${WEB_PATH }/static/Address/main.js"></script>
	<script>
		$(function() {
			document.getElementById("introduction").value = "${user.introduction }";
			var sexs;
			document.getElementById("sex").value = "${user.sex }";

			$('#alter-info').on('click', function() {
				$("input[readonly='readonly']").removeProp("readonly");
				$("textarea[readonly='readonly']").removeProp("readonly");
				$('#alter-confirm').attr("disabled", false);
				$('#sex').attr("disabled", false);
			});

		});
	</script>
</body>
</html>
