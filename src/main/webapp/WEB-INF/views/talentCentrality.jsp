<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- Google Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="${WEB_PATH }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="${WEB_PATH }/static/css/owl.carousel.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/style.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/responsive.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/navigationSide.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/app.css">

</head>
<body>
	<%@include file="layout/navigation.jsp"%>
	<%@include file="layout/talentNavigationSide.jsp"%>
	
	<!-- Latest jQuery form server -->
	<script src="https://code.jquery.com/jquery.min.js"></script>

	<!-- Bootstrap JS form CDN -->
	<script
		src="${WEB_PATH }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

	<!-- jQuery sticky menu -->
	<script src="${WEB_PATH }/static/js/owl.carousel.min.js"></script>
	<script src="${WEB_PATH }/static/js/jquery.sticky.js"></script>

	<!-- jQuery easing -->
	<script src="${WEB_PATH }/static/js/jquery.easing.1.3.min.js"></script>

	<!-- Main Script -->
	<script src="${WEB_PATH }/static/js/main.js"></script>

	<script src="${WEB_PATH }/static/Address/distpicker.data.min.js"></script>
	<script src="${WEB_PATH }/static/Address/distpicker.min.js"></script>
	<script src="${WEB_PATH }/static/Address/main.js"></script>
</body>
</html>