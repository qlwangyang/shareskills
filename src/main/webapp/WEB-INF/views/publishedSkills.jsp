<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>达人展示</title>
<!-- Google Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="${WEB_PATH }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- head  -->
<%@include file="layout/navigation.jsp"%>
<%@include file="layout/select.jsp"%>
<!-- Custom CSS -->
<link rel="stylesheet" href="${WEB_PATH }/static/css/owl.carousel.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/style.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/responsive.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/navigationSide.css">
<link rel="stylesheet" href="${WEB_PATH }/static/css/app.css">
<link rel="stylesheet"
	href="${WEB_PATH }/static/css/publishedSkills.css">


</head>
<body>
	<div class="maincontent-area">
		<div class="zigzag-bottom"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="latest-product">
						<!-- 在这里写foreach方法 展示技能达人图片 -->
						<div class="product-carousel">
							<c:forEach items="${publishedSkillsVOs }" var="p">
								<div class="single-product">
									<div class="product-f-image">
										<img src="${WEB_PATH }/static/img/product-1.jpg" alt="">
										<div class="product-hover">
											<a href="function:;" class="add-to-cart-link"
												onclick="attention(this)"> <c:choose>
													<c:when test="1111">
														<i class="fa fa-heart-o"></i>
														<span>关注他/她</span>
													</c:when>
													<c:otherwise>
														<i class="fa fa-heart"></i>
														<span>已关注</span>
													</c:otherwise>
												</c:choose>
											</a> <a href="single-product.html" class="view-details-link"><i
												class="fa fa-link"></i> <span>他/她的主页</span></a>
										</div>
									</div>

									<h2>
										<a href="single-product.html"></a>
									</h2>

									<div class="product-carousel-price">
										<h4>${p.name}</h4>
										<ins>${p.description }</ins>
										<!-- <del>$800.00</del> -->
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
			<h2 class="section-title">
				<a href="function:;" class="btn btn-success">更多技能达人</a>
			</h2>
		</div>
	</div>

	<!-- End main content area -->



</body>
<!-- Latest jQuery form server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script
	src="${WEB_PATH }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="${WEB_PATH }/static/js/owl.carousel.min.js"></script>
<script src="${WEB_PATH }/static/js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="${WEB_PATH }/static/js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="${WEB_PATH }/static/js/main.js"></script>

<script src="${WEB_PATH }/static/Address/distpicker.data.min.js"></script>
<script src="${WEB_PATH }/static/Address/distpicker.min.js"></script>
<script src="${WEB_PATH }/static/Address/main.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/easing.js"></script>
<script
	src="${WEB_PATH }/static/publishedSkills/js/jquery.etalage.min.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/jquery.flexisel.js"></script>
<script
	src="${WEB_PATH }/static/publishedSkills/js/jquery.jscrollpane.min.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/jquery.min.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/jquery.wmuSlider.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/megamenu.js"></script>
<script src="${WEB_PATH }/static/publishedSkills/js/move-top.js"></script>
<script src="${WEB_PATH }/static/js/publishedSkills.js"></script>
</html>