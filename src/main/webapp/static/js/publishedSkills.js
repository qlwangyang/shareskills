
$(function() {
	$('.rng1-2').click(function() {
		$(this).parent().siblings('.rng2').toggle();
		return false;
	});
	$('.rng4').mouseover(function(event) {
		$(this).find('h3').css('color', '#FF946E');
		event.stopPropagation();
	});
	$('.rng4').mouseout(function(event) {
		$(this).find('h3').css('color', '#000000');
		event.stopPropagation();
	});
});